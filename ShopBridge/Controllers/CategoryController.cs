﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ShopBridge.Models;

namespace ShopBridge.Controllers
{
    public class CategoryController : ApiController
    {
        ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        public PageResponse GetCategoryList(long pageSize = 0, long page = 0)
        {
            try
            {
                long PageSize = pageSize <= 0 ? 100 : pageSize;
                long Page = page <= 0 ? 1 : page;
                var CategoryList = db.category.Select(x => new
                {
                    x.CategoryId,
                    x.CategoryName,
                    x.IsActive
                }).ToList();

                long totalRecords = CategoryList.Count;

                long totalPages = (long)Math.Ceiling((totalRecords / (double)PageSize));

                CategoryList = CategoryList.Skip((int)((Page - 1) * pageSize)).Take((int)PageSize).ToList();

                return new PageResponse
                {
                    StausCode = 1,
                    Message = "Success",
                    Record = CategoryList,
                    Pagesize = PageSize,
                    Page = Page,
                    TotalPages = totalPages,
                    TotalRecords = totalRecords
                };
            }
            catch (Exception ex)
            {
                return new PageResponse { StausCode = 0, Message = ex.Message, Record = null };
            }
        }

        [HttpGet]
        public Response GetCategory(long id)
        {
            try
            {
                if (id < 0)
                {
                    return new Response { StausCode = 0, Message = "Id is required", Record = null };
                }

                var CategoryDetails = db.category.Where(x => x.CategoryId == id).Select(x => new
                {
                    x.CategoryId,
                    x.CategoryName,
                    x.IsActive

                }).FirstOrDefault();
                if (CategoryDetails == null)
                {
                    return new Response { StausCode = 0, Message = "No such a category..", Record = null };
                }
                return new Response { StausCode = 1, Message = "Success", Record = CategoryDetails };
            }
            catch (Exception ex)
            {
                return new Response { StausCode = 0, Message = ex.Message, Record = null };
            }
        }


        [HttpPost]
        public Response AddEditCategory(Category category)
        {
            if (category == null)
            {
                return new Response { StausCode = 0, Message = "Bad Request", Record = null };
            }

            if (string.IsNullOrWhiteSpace(category.CategoryName))
            {
                return new Response { StausCode = 0, Message = "Category Name is required", Record = null };
            }

            if (category.CategoryId > 0)
            {
                var categoryData = db.category.Where(x => x.CategoryId == category.CategoryId && x.IsActive == true).FirstOrDefault();
                if (categoryData == null)
                {
                    return new Response { StausCode = 0, Message = "No such category", Record = null };
                }
                categoryData.CategoryName = category.CategoryName;
                db.SaveChanges();
                return new Response
                {
                    StausCode = 1,
                    Message = "Category Updated successfully",
                    Record = new
                    {
                        CategoryId = categoryData.CategoryId,
                        CategoryName = categoryData.CategoryName
                    }
                };
            }
            else
            {
                Category category1 = new Category
                {
                    CategoryName = category.CategoryName,
                    IsActive = category.IsActive
                };
                db.category.Add(category1);
                db.SaveChanges();

                return new Response
                {
                    StausCode = 1,
                    Message = "Category Added successfully",
                    Record = new
                    {
                        CategoryId = category1.CategoryId,
                        CategoryName = category1.CategoryName
                    }
                };
            }
        }

        [HttpPost]
        public Response DeleteCategory(Category categoryData)
        {
            try
            {
                if (categoryData == null && categoryData.CategoryId <= 0)
                {
                    return new Response { StausCode = 0, Message = "Category Id is required", Record = null };
                }

                var category = db.category.Where(x => x.CategoryId == categoryData.CategoryId).FirstOrDefault();
                if (category == null)
                {
                    return new Response { StausCode = 0, Message = "No such Category", Record = null };
                }

                db.category.Remove(category);
                db.SaveChanges();
                return new Response { StausCode = 1, Message = "Category deleted successfully", Record = null };

            }
            catch (Exception ex)
            {
                return new Response { StausCode = 0, Message = ex.Message, Record = null };
            }

        }
    }
}
