﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ShopBridge.Models;

namespace ShopBridge.Controllers
{
    public class InventoryController : ApiController
    {

        public ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        public PageResponse GetInventoryList(long pageSize = 0, long page = 0)
        {
            try
            {
                long PageSize = pageSize <= 0 ? 100 : pageSize;
                long Page = page <= 0 ? 1 : page;
                var InventoryList = db.inventory.Select(x => new
                {
                    x.InventorId,
                    x.InventoryName,
                    x.Price,
                    x.Stock,
                    x.IsDeleted,
                    x.category.CategoryName
                }).ToList();

                long totalRecords = InventoryList.Count;

                long totalPages = (long)Math.Ceiling((totalRecords / (double)PageSize));

                InventoryList = InventoryList.Skip((int)((Page - 1) * pageSize)).Take((int)PageSize).ToList();

                return new PageResponse
                {
                    StausCode = 1,
                    Message = "Success",
                    Record = InventoryList,
                    Pagesize = PageSize,
                    Page = Page,
                    TotalPages = totalPages,
                    TotalRecords = totalRecords
                };
            }
            catch (Exception ex)
            {
                return new PageResponse { StausCode = 0, Message = ex.Message, Record = null };
            }
        }

        [HttpGet]
        public Response GetInventory(long id)
        {
            try
            {
                if (id < 0)
                {
                    return new Response { StausCode = 0, Message = "Id is required", Record = null };
                }

                var inventoryDetails = db.inventory.Where(x => x.InventorId == id).Select(x => new
                {
                    x.InventoryName,
                    x.Stock,
                    x.Price,
                    x.IsDeleted,
                    x.category.CategoryName

                }).FirstOrDefault();
                if (inventoryDetails == null)
                {
                    return new Response { StausCode = 0, Message = "No such a inventory..", Record = null };
                }
                return new Response { StausCode = 1, Message = "Success", Record = inventoryDetails };
            }
            catch (Exception ex)
            {
                return new Response { StausCode = 0, Message = ex.Message, Record = null };
            }
        }

        [HttpPost]
        public Response AddEditInventory(Inventory inventory)
        {
            try
            {
                if (inventory == null)
                {
                    return new Response { StausCode = 0, Message = "Bad Request", Record = null };
                }

                if (string.IsNullOrWhiteSpace(inventory.InventoryName))
                {
                    return new Response { StausCode = 0, Message = "Inventory Name is required", Record = null };
                }
                if (inventory.Stock < 0)
                {
                    return new Response { StausCode = 0, Message = "Stock is required", Record = null };
                }

                if (inventory.Price < 0)
                {
                    return new Response { StausCode = 0, Message = "Please Enter valid price", Record = null };
                }

                if (inventory.CategoryId < 0)
                {
                    return new Response { StausCode = 0, Message = "Please select Category", Record = null };
                }

                if (inventory.InventorId > 0)
                {
                    var inventoryData = db.inventory.Where(x => x.InventorId == inventory.InventorId).FirstOrDefault();
                    if (inventoryData != null)
                    {
                        inventoryData.InventoryName = inventory.InventoryName;
                        inventoryData.Stock = inventory.Stock;
                        inventoryData.Price = inventory.Price;
                        inventoryData.CategoryId = inventory.CategoryId;
                        inventoryData.IsDeleted = inventory.IsDeleted;
                        db.SaveChanges();
                        return new Response
                        {
                            StausCode = 1,
                            Message = "Inventory Updated Successfully.",
                            Record = GetInventory(inventoryData.InventorId).Record
                        };
                    }
                    else
                    {
                        return new Response { StausCode = 0, Message = "No such Inventory.", Record = null };
                    }
                }
                else
                {
                    Inventory inventoryData1 = new Inventory
                    {
                        InventoryName = inventory.InventoryName,
                        Stock = inventory.Stock,
                        Price = inventory.Price,
                        IsDeleted = inventory.IsDeleted,
                        CategoryId = inventory.CategoryId
                    };
                    db.inventory.Add(inventoryData1);
                    db.SaveChanges();

                    return new Response
                    {
                        StausCode = 1,
                        Message = "Inventory Added Successfully.",
                        Record = GetInventory(inventoryData1.InventorId).Record
                    };
                }
            }
            catch (Exception ex)
            {
                return new Response { StausCode = 0, Message = ex.Message, Record = null };
            }
        }

        [HttpPost]
        public Response DeleteInventory(Inventory inventoryData)
        {
            try
            {
                if (inventoryData == null && inventoryData.InventorId <= 0)
                {
                    return new Response { StausCode = 0, Message = "Inventory Id is required", Record = null };
                }

                var inventory = db.inventory.Where(x => x.InventorId == inventoryData.InventorId).FirstOrDefault();
                if (inventory == null)
                {
                    return new Response { StausCode = 0, Message = "No such Inventory", Record = null };
                }

                db.inventory.Remove(inventory);
                db.SaveChanges();
                return new Response { StausCode = 1, Message = "Inventory deleted successfully", Record = null };

            }
            catch (Exception ex)
            {
                return new Response { StausCode = 0, Message = ex.Message, Record = null };
            }

        }

    }
}
