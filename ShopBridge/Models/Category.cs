﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopBridge.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }


        public string CategoryName { get; set; }

        public bool IsActive { get; set; }

        public ICollection<Inventory> inventory { get; set; }
    }
}