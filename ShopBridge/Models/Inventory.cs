﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopBridge.Models
{
    public class Inventory
    {
        [Key]
        public int InventorId { get; set; }

        public string InventoryName { get; set; }

        public decimal Price { get; set; }

        public int Stock { get; set; }

        public bool IsDeleted { get; set; }

        public int CategoryId { get; set; }

        public virtual Category category { get; set; }
    }
}