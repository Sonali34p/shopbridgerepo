﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopBridge.Models
{
    public class PageResponse
    {
        public int StausCode { get; set; }
        public string Message { get; set; }
        public object Record { get; set; }
        public long Pagesize { get; set; }
        public long Page { get; set; }
        public long TotalPages { get; set; }
        public long TotalRecords { get; set; }

    }
}