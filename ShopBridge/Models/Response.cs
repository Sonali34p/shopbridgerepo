﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopBridge.Models
{
    public class Response
    {
        public int StausCode { get; set; }
        public string Message { get; set; }
        public object Record { get; set; }
    }
}